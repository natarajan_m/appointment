﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AppointmentService } from '../_services';

@Component({templateUrl: 'appointment.component.html'})
export class AppointmentComponent implements OnInit {
    appointmentForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private appointmentService: AppointmentService,
        private alertService: AlertService) { }

    ngOnInit() {
        this.appointmentForm = this.formBuilder.group({
            appointmentProvider: ['' || localStorage.currentUser.username, Validators.required],
            appointmentSeeker: ['', Validators.required],
            appointmentDate: ['', Validators.required],
			appointmentTime: ['', Validators.required],
			appointmentTimezone: ['', Validators.required],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.appointmentForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.appointmentForm.invalid) {
			console.log("Failed",this.appointmentForm.value);
            return;
        }

        this.loading = true;
        this.appointmentService.create(this.appointmentForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Appointment Schedule is successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
