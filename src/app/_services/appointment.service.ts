﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Appointment } from '../_models';

@Injectable()
export class AppointmentService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Appointment[]>('/api/appointments');
    }

    getById(appointmentId: number) {
        return this.http.get('/api/appointments/' + appointmentId);
    }

    create(appointment: Appointment) {
        return this.http.post('/api/appointments', appointment);
    }

    update(appointment: Appointment) {
        return this.http.put('/api/appointments/' + appointment.appointmentId, appointment);
    }

    delete(appointmentId: number) {
        return this.http.delete('/api/appointments/' + appointmentId);
    }
}