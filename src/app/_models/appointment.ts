﻿export class Appointment {
    appointmentId: number;
    appointmentProvider: string;
    appointmentSeeker: string;
    appointmentDate: string;
	appointmentTime: string;
    appointmentTimezone: string;	
}