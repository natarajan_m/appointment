﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Appointment } from '../_models';

import { AlertService, AppointmentService,UserService } from '../_services';

@Component({templateUrl: 'appointmentview.component.html'})
export class AppointmentViewComponent implements OnInit {
    appointments: Appointment[] = [];
	currentUser :any ;
    constructor(private userService: UserService, private appointmentService:AppointmentService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllAppointments();
    }   

    private loadAllAppointments() {
        this.appointmentService.getAll().pipe(first()).subscribe(appointments => { 
            this.appointments = appointments; 
        });
    }
}
